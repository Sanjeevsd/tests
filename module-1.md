#  Introdue yourself

Hello, I am Sanjeev Das from Nepal.I am final year(7th semester) computer engineering student in Kantipur engineering college.I have only two main hobbies, programming and Rubik's cube solving.I am physically weak for outdoor games, so I have interest in only indoor activities.

I started programming since grade 8(QBASIC), since then I always loved solving problems through programming, but the place where I am from didnt had any resources through which I could learn.The first time I got access to internet was in my highschool final year.Since then I always searched and learned different programming skills  myself.This continued till now, final year of my bachelor's degree.So I want to share my knowledge and help students who dont have much knowledge in programming by building a community in my college.In my college or others college near by me, the students dont have any idea even about git or other mainstream programming languages, I learned about git last year, which I will be teaching to my juniors soon along with many other things, through the community which I statred this semester.

# Community Defination

The community I am a part of curently is 'Open Source Community KEC', which is a computer science club.Our community is not officially recognized by our college or official Computer and Electronics club of our college yet, because we started this community just a month ago, and we will be doing the first event from our community in 5th jan 2020.
I built this community together with friends from same batch, we share common interests.We want to share and learn together with other students within our college.Since we started our community only a month ago, it has currently 10 active members, with other inactive juniors who are interested in taking part in the upcomming event in january 5.Initially I started with 4 friends and it reached to 10 members within this month.

# What are your community's shared struggles?

The students in our college dont have much resources to learn from.The college course only has few programming topics.Even if the students want to learn there is no any resourses or group where they can learn from, so i along with my community want to create a environment from where students can gain skills and knowledge which will help them in future to get jobs or choose right path in their career.There are very few events like hackathons or worshops in our city, even if there are any workshops, they are paid workshops which avarage student cannot afford to attend.These problems will help bring our community together and get bigger over time, since our community main objetcive is to solve such probelms.

# what is the mission of your community?

We will organise events like hackathons, coding camps and workshops which will help students gain skills and knowledge in problem solving together with programming skills.This will help them get a better job in future or help them to choose right path in future.We will be doing every events voluntarily so it will be free or minimal charge and attract many students.
The events will also help them in completing the engineering course, since students have to submit two projects till final year,our community will help them choose better projects and get desired output.

# What members look to get from your community?

## Activities
We will organise basic workshops to help students get concepts in a particular programming language.We will also hold coding challenge events, which will hone students skills.During such events the organising team will help them solve their respective problems.Also our community will help students in completing projects by assisting them.The projects are mandatory to pass the college.By organising such events, students will be able to reach their goal as well as hone their skills.
## Communication
The main medium of communication currently is a facebook group, we are also planning to hold meetings on friday which is a holiday in our college.Since  our college is not that big and the members are of the same college, we can easily get in touch within college grounds.After the event of january 5(Introduction to Linux and Git), we were planning to open a discord channel,but one friend suggested Slack which is used by many prgrammers and event organisers.We are planning to announce about the channel during the event.

# Future-proofing your communnity

I still have one whole year in this college, within which i will do my best for this community.After my graduation i will be still in touch with the community and support it as much as possible.

## Agency
Our community is filled with members with same interests but with knowledge in many different fields, some interested in web developing, some app development and some in graphics designing.People who are good at thier skills will be asked to organise a event and the community will help in oganising it.Some people will be given task in managing the group or event managing.We will be open to feedback from the members, if they want us to organise specific events we will do that too.
## Mission
We will be observing the members behaviour,skills and their goal this year,so that when we senior graduate we will be able to select the best leader for the community who will work on the community mission.

## Structure

Our community will be lead by senior members(4th year students).We have formed a structure with 7 board members,who will help making decisions and manage the community.As mentioned above, we will choose leadership successor based on their contributions to this community within this year and also their leadership skills.The Successor will be choosed only from 3rd year who will be promoted to 4th year next year.This is because the senior members will have more knowledge and experience compared to juniors.This does not mean that the board members will only contain students from 3rd years(getting promoted to 4th years).The junior students will also be selected to be part of leading team if they have good leadership skills. 
We current leaders wil make ourself available for advice if the new leaders want it,we won't stand in their ways and we will be checking on them time to time if they are doing things perfectly, without trying to hang on to something which we have already handedover.












