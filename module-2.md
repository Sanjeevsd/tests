 
# Who might be excluded in accessing your communities in their current form?

We are open to all students,who are eager to learn more and hone their skills,unless they are senior graduate students, or students who are curently doing interns as they don't have time to spend in community works.
Any one from inside or outside our campus are openly invited to join our community unless they violate the code of conducts of our community.People around our city are somewhat focused on racisim, so we strongly discourage it, and people/students who practice such activities wont be given access to our community activities.We are open to students who want to learn,but there might be students who will join our community events/activities to bunk classes, which we strongly discourage.

# Handliing negative scenarios

## What are some of the negative scenarios which might happen while running activities for your community which will make them less inclusive to marginalized groups?

Less participation of girls, For reference in my class we have 40 students out of which 7 are girls.Since there are few girls, they hesitate to praticipate in events consisiting of huge number of boys.

We mostly conduct events in our local language 'Nepali', so it will be difficult for participants who wont understand our language.

Nepal is one of the county which still practice caste system in some parts of country, so most people belonging to so called lower caste are not given much priority and are excluded from major events.Actually I myself belong to the Scheduled Caste(Dalits), and in the past i also had faced such problems.

With growing technologies, childrens are very fond of technologies so they might want to participate in our community events, but since they are much less skilled and experienced they might not be able get chanace to participate with other seniors.

## What steps will you take to avoid these scenarios?

Our college has a Girls cummunity, we will collaborate with them to increase girls participation in the community activities.

Translaters can be appointed during community activities to translate from Nepali lanugage to other languages, or we can even conduct events in English.

We will encourage every caste people to participate in our activities, and even give exclusive gifts(stickers, discounts) to lower caste people.If anyone is found discriminating then they will be disqualified from the event and removed from the community.

It will be difficult to manage undergraduate students,highschool students with junior students, so we will do specific events targeting childrens only.

# Designing for your community's needs

## Needs faciliate
We will try to faciliate needs such as, wheelchairs, translators, canteen service, big enough halls to participate with ease.

## Needs not faciliate
We wont be able to provide transportation service, because it will require huge budget which a voulnteering community like ours cannot afford.We wont be able to 

#Provide a link to your Code of Conduct
We currentyl only have a facebook group : [Kec OSC](https://www.facebook.com/groups/975874796127561/)

The code of conducts:
[Code of conduct](https://gist.github.com/Sanjeevsd/3a00643333af936636304f6e25ff4ff7)
